# Changelog

## 4.0.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+