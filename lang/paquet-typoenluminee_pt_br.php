<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-typoenluminee?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// T
	'typoenluminee_description' => 'Este plugin adiciona atalhos tipográficos e botões ao porte plume. Por exemplo:
-* Parágrafos:
-** {{Subtítulos}} <code>{{{**</code>Texto do título<code>}}}</code>, o número de estrelas (*) variando de 2 a 5, a barra de atalhos só oferece  2 e 3
-** {{Centrar}} <code>[|</code>Parágrafo centrado<code>|]</code>
-** {{Alinhar à direita}} <code>[/</code>Parágrafo alinhado à direita<code>/]</code>
-** {{Enquadrar}} <code>[(</code>Parágrafo a enquadrar<code>)]</code>
-* Caracteres:
-** {{Destacar}} <code>[*</code>texto destacado<code>*]</code>
-** {{Destacar alternativo}} <code>[**</code>texto destacado (outra cor)<code>*]</code>
-** {{Sobrescrito}} {&lt;sup&gt;}texto em sobrescrito{&lt;/sup&gt;}
-** {{Caixa alta-baixa}} {&lt;sc&gt;}texto em caixa alta-baixa{&lt;/sc&gt;}
-** {{Tachado}} {&lt;del&gt;}texto tachado{&lt;/del&gt;}',
	'typoenluminee_slogan' => 'Cheio de atalhos para a formatação!',
];
