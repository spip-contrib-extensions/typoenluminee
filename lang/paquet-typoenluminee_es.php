<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-typoenluminee?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// T
	'typoenluminee_description' => 'Este complemento agrega atajos y botones tipográficos al portalápices. Por ejemplo:
-* Párrafos :
-** {{subtítulos}} <code>{{{**</code>Texto del título<code>}}}</code>, y número de asteriscos (*) varía de 2 a 5, la barra de acceso directo solo ofrece 2 y 3
-** {{Centrar}} <code>[|</code>Párrafo centrado<code>|]</code>
-** {{alinear a la derecha}} <code>[/</code>Párrafo alineado a la derecha<code>/]</code>
-** {{Enmarcar}} <code>[(</code>Párrafo para enmarcar<code>)]</code>
-* Caracteres :
-** {{Resaltar}} <code>[*</code>resaltar texto<code>*]</code>
-** {{Resaltar variante}} <code>[**</code>resaltar texto (otro color)<code>*]</code>
-** {{Sobrescrito}} {&lt;sup&gt;}texto sobre escrito{&lt;/sup&gt;}
-** {{Versalitas}} {&lt;sc&gt;}texto en versalitas{&lt;/sc&gt;}
-** {{Subrayado}} {&lt;del&gt;}texto subrayado{&lt;/del&gt;}

',
	'typoenluminee_slogan' => '¡Muchos atajos para variar!',
];
