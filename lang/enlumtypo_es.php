<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/enlumtypo?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// B
	'barre_alignerdroite' => '[/Alínea a la derecha/] el párrafo',
	'barre_alignergauche' => '[!Alinear a la izquierda!] el párrafo',
	'barre_avances' => '¡Significado, significado!',
	'barre_barre' => '<tachar/> el texto',
	'barre_boutonsavances' => '¡Significados adicionales, para ser usados ​​con moderación y discernimiento!',
	'barre_cadre' => 'Colocar en un <frame>cuadro de entrada de texto</frame>',
	'barre_centrer' => '[|Centrar|] el párrafo',
	'barre_code' => 'Formatear un <code>código de computadora</code>',
	'barre_encadrer' => '[(Encuadrar)] el párrafo',
	'barre_exposant' => 'Convertir el texto en <sup>superíndice</sup>',
	'barre_formatages_speciaux' => 'Formato especial',
	'barre_indice' => 'Poner texto en <sub>subíndice</sub>',
	'barre_intertitre2' => 'Transformar en {2{intertítulos nivel dos}2}',
	'barre_intertitre3' => 'Transformar en {3{intertítulos nivel tres}3}',
	'barre_miseenevidence' => 'Poner el texto en [*evidencia*]',
	'barre_miseenevidence2' => 'Poner el texto en [**resaltado*] (segundo color)',
	'barre_petitescapitales' => 'Poner el texto en &lt;sc&gt;mayúsculas pequeñas&lt;/sc&gt;',
	'barre_poesie' => 'Formato como <poesie>poesía</poesie>',
	'barre_tableau' => 'Insertar/modificar (seleccionarlo antes) una tabla',
	'bouton_reinitialiser' => 'Reiniciar',

	// C
	'cfg_insertcss' => 'Introducir CSS',
	'cfg_puces' => 'Tratamiento de "puces"',
	'cfg_titraille' => 'Titular',
	'configuration_typoenluminee' => 'Adornos tipográficos',

	// T
	'tableau_enregistrer' => 'Registro',
	'tableau_prem_ligne' => 'Primera linea',
	'tableau_resume' => 'Reanudar',
	'tableau_titre' => 'Título',
];
